abstract class SolverRuntimeError<TUnderlyingError extends SolverError = SolverError> extends Error{

    public readonly __err_type = 'solver-runtime-error';
    public readonly underlyingError: TUnderlyingError|null;

    /**
     * @param {TUnderlyingError | null} underlyingError
     * @param {string} message
     */
    public constructor(underlyingError: TUnderlyingError|null = null, message?: string) {
        super(message);
        this.underlyingError = underlyingError;
    }

}

export class SolverError<TErrorCode extends any = any> extends SolverRuntimeError<never>{

    public readonly errorCode: TErrorCode|null;

    /**
     * @param {TErrorCode | null} errorCode
     * @param {string} message
     */
    public constructor(errorCode: TErrorCode|null, message?: string) {
        super(null, message);
        this.errorCode = errorCode;
    }

}

export class SolverCreateTaskError<TUnderlyingError extends SolverError = SolverError> extends SolverRuntimeError<TUnderlyingError>{

}

export class SolverGetTaskResultError<TUnderlyingError extends SolverError = SolverError> extends SolverRuntimeError<TUnderlyingError>{

}
