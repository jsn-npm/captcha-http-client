import {CaptchaOptionsAny, CaptchaSolutionAny} from '@captchas';
import {SolverCreateTaskError, SolverGetTaskResultError} from './errors';
import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';
import {wait} from '@utils';

export abstract class Solver<
    TaskId extends (string|number)
>{

    protected readonly accessToken: string;
    protected readonly httpClient: AxiosInstance;

    /**
     * @param {string} accessToken
     */
    public constructor(accessToken: string) {
        this.accessToken = accessToken;
        this.httpClient = axios.create(this.makeHTTPClientConfig({
            baseURL: this.getAPIURL()
        }));
    }

    /**
     * @param {AxiosRequestConfig} defaultConfig
     * @return {AxiosRequestConfig}
     * @protected
     */
    protected makeHTTPClientConfig(defaultConfig: AxiosRequestConfig): AxiosRequestConfig{
        return defaultConfig;
    }

    /**
     * @return {string}
     * @protected
     */
    protected abstract getAPIURL(): string;

    /**
     * @param {CaptchaOptionsAny["type"]} type
     * @return {number}
     * @protected
     */
    protected abstract getMaxAttempts(type: CaptchaOptionsAny['type']): number;

    /**
     * @param {CaptchaOptionsAny["type"]} type
     * @return {number}
     * @protected
     */
    protected abstract getRetryInterval(type: CaptchaOptionsAny['type']): number;

    /**
     * @param {O} captcha
     * @return {Promise<R>}
     */
    public async solve<
        O extends CaptchaOptionsAny,
        R extends CaptchaSolutionAny & {type: O['type']}
    >(captcha: O): Promise<R>{
        let taskId: TaskId|null = null;
        try{
            taskId = await this.createTask(captcha);
        }catch(e){
            if(e && e.__err_type && typeof e.__err_type === 'string' && e.__err_type === 'solver-runtime-error')
                throw new SolverCreateTaskError(e);

            throw new SolverCreateTaskError(null, 'Failed to create task');
        }

        const maxAttempts = this.getMaxAttempts(captcha['type']),
            retryInterval = this.getRetryInterval(captcha['type']);

        let attempt: number = 1;
        while(attempt <= maxAttempts){
            await wait(retryInterval);
            let response: CaptchaSolutionAny|null = null;
            try{
                response = await this.getTaskResult(captcha['type'], taskId);
            }catch(e){
                if(e && e.__err_type && typeof e.__err_type === 'string' && e.__err_type === 'solver-runtime-error')
                    throw new SolverGetTaskResultError(e);

                throw new SolverGetTaskResultError(null, 'Failed to obtain task result');
            }

            if(response !== null)
                return response as R;

            attempt++;
        }

        throw new SolverGetTaskResultError(null, 'Attempts exceeded');
    }

    /**
     * @param {O} options
     * @return {Promise<TaskId>}
     * @protected
     */
    protected abstract createTask<O extends CaptchaOptionsAny>(options: O): Promise<TaskId>;

    /**
     * @param {S["type"]} type
     * @param {TaskId} taskId
     * @return {Promise<S | null>}
     * @protected
     */
    protected abstract getTaskResult<S extends CaptchaSolutionAny>(type: S['type'], taskId: TaskId): Promise<S|null>;

}
