import {Solver} from '@core';
import {CaptchaOptionsAny, CaptchaSolutionAny} from '@captchas';
import {RucaptchaError} from './errors';

export class Rucaptcha extends Solver<number>{

    /**
     * @return {string}
     * @protected
     */
    protected getAPIURL(): string {
        return 'https://rucaptcha.com';
    }

    /**
     * @param {CaptchaOptionsAny["type"]} type
     * @return {number}
     * @protected
     */
    protected getMaxAttempts(type: CaptchaOptionsAny['type']): number {
        return type === 'ImageCaptcha' ? 6 : 3;
    }

    /**
     * @param {CaptchaOptionsAny["type"]} type
     * @return {number}
     * @protected
     */
    protected getRetryInterval(type: CaptchaOptionsAny['type']): number {
        return type === 'ImageCaptcha' ? 5000 : 20000;
    }

    /**
     * @param {O} options
     * @return {Promise<number>}
     * @protected
     */
    protected async createTask<O extends CaptchaOptionsAny>(options: O): Promise<number> {
        const body: Record<string, any> = {
            'key': this.accessToken
        };

        if(options.proxy){
            let proxyString = '';
            if(options.proxy.user && options.proxy.password)
                proxyString += `${options.proxy.user}:${options.proxy.password}@`;

            proxyString += `${options.proxy.ip}:${options.proxy.port}`;

            body['proxy'] = proxyString;
            body['proxytype'] = options.proxy.proto.toUpperCase();
        }

        switch(options.type){
            case 'ImageCaptcha': {
                body['method'] = 'base64';
                body['body'] = options.body;

                if(options.isCaseSensitive !== undefined)
                    body['regsense'] = options.isCaseSensitive ? 1 : 0;

                if(options.isOnlyDigits !== undefined)
                    body['numeric'] = options.isOnlyDigits ? 1 : 0;

                if(options.isMath !== undefined)
                    body['calc'] = options.isMath ? 1 : 0;
            } break;
            case 'ReCaptcha': {
                body['method'] = 'userrecaptcha';
                body['googlekey'] = options.site.key;
                body['pageurl'] = options.site.url;

                if(options.version === 2){
                    if(options.isInvisible !== undefined)
                        body['invisible'] = options.isInvisible ? 1 : 0;

                    body['data-s'] = options.sValue;
                }else{
                    if(options.pageAction !== undefined)
                        body['action'] = options.pageAction;

                    body['min_score'] = options.minScore;
                    body['version'] = 'v3';
                }
            } break;
            default: throw new RucaptchaError(null, `${options['type']} is not supported`);
        }

        const response = await this.httpClient.post<string>('/in.php', body);
        if(!/^OK|\d+$/.test(response.data))
            throw new RucaptchaError(response.data, 'Bad response');

        return parseInt(response.data.split('|')[1]);
    }

    /**
     * @param {S["type"]} type
     * @param {number} taskId
     * @return {Promise<T | null>}
     * @protected
     */
    protected async getTaskResult<S extends CaptchaSolutionAny>(type: S['type'], taskId: number): Promise<S | null> {
        const response = await this.httpClient.get<string>(`/res.php?key=${this.accessToken}&action=get&id=${taskId}&json=0`);

        if(/^CAPT?CHA_NOT_READY$/iu.test(response.data))
            return null;

        if(/^ERROR_/iu.test(response.data))
            throw new RucaptchaError(response.data);

        if(!/^OK|/iu.test(response.data))
            throw new RucaptchaError(null, 'Unknown error');

        return {
            type: type,
            solution: response.data.split(/^OK\|/iu)[1]
        } as any;
    }

}
