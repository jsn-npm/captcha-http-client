import {Rucaptcha} from '@solvers/rucaptcha';

(async () => {
    const solver = new Rucaptcha('8479b6ea7d354fda88f26c05db8b4db2');
    const solution = await solver.solve({
        type: 'ReCaptcha',
        version: 2,
        site: {
            url: 'https://www.google.com/sorry/index?continue=https://google.com/&q=EgQtgrlGGI_x4qQGIjCeWyJf4tBtrBhPh2oxtU5BY1NcxgCM_kkAl0-TxZAwDFXNaIniy6q_qASmDkuvXFwyAXJaAUM',
            key: '6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1b'
        },
        sValue: 'dVuzKd4VnGOOxD9OOpY-obULv4dXtpaSUInGVjcj4yqmEuNkinb6fAhhjqjQXZ2OvjAXHLa6avtrwydgpArGfYIjlT7gw-kZUYof8VW2FLrsFJTkb_UqKVX1Gfgbr8IqM1dNN3XDkDTF-IUVPveYVgwQ_8Sv4OWz_0-yokyIiD3gprA1tKW979vC82aLuDsHuXQ_nZsHle-FGGKRX-tp-H8SJcR78tWxzXAlJzzN1NXKBQqW5V6lkDajL8nhqsqJRyXjxc6vS525UHPtTstAhMJx9J6Yxf0',
        isInvisible: true
    });

    console.log('Solution', solution);
})();
