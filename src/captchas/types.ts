export type CaptchaOptionsBase<T extends string, C extends any> = {
    type: T;
} & C;

export type CaptchaSolutionBase<T extends string, C extends any> = {
    type: T;
    solution: C;
};

export type ProxyConnectionData = {
    proto: 'http'|'https'|'socks5';
    ip: string;
    port: number;
    user?: string;
    password?: string;
};
