import {ImageCaptchaOptions, ImageCaptchaSolution} from './ImageCaptcha';
import {ReCaptchaOptions, ReCaptchaSolution} from './ReCaptcha';

export type CaptchaOptionsAny =
    ImageCaptchaOptions
    | ReCaptchaOptions;

export type CaptchaSolutionAny =
    ImageCaptchaSolution
    | ReCaptchaSolution;
