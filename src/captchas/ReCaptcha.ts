import {CaptchaOptionsBase, CaptchaSolutionBase, ProxyConnectionData} from './types';

type ReCaptchaBase = {
    version: 2|3;
    site: {
        url: string;
        key: string;
    };

    proxy?: ProxyConnectionData;
};

type ReCaptchaV2 = ReCaptchaBase & {
    version: 2;
    sValue?: string;
    isInvisible?: boolean;
}

type ReCaptchaV3 = ReCaptchaBase & {
    version: 3;
    minScore?: number;
    pageAction?: string;
};

export type ReCaptchaOptions = CaptchaOptionsBase<'ReCaptcha', ReCaptchaV2 | ReCaptchaV3>;
export type ReCaptchaSolution = CaptchaSolutionBase<'ReCaptcha', string>;
