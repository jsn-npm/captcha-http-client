import {CaptchaOptionsBase, CaptchaSolutionBase, ProxyConnectionData} from './types';

export type ImageCaptchaOptions = CaptchaOptionsBase<'ImageCaptcha', {
    body: string;

    isCaseSensitive?: boolean;
    isOnlyDigits?: boolean;
    isMath?: boolean;

    proxy?: ProxyConnectionData;
}>;

export type ImageCaptchaSolution = CaptchaSolutionBase<'ImageCaptcha', string>;
